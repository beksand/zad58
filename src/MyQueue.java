import java.util.ArrayList;
//Stwórz strukturę FIFO która posiada metody:
//            - push_back - dodawanie wartości (na koniec)
//        - pop_front - wyciąganie wartości (wartość jest zwrócona) (z początku)
//            - peek - zwraca następną wartość ale jej nie usuwa (podgląda)
//        - size - zwraca rozmiar struktury
//        - push_front - dodanie na koniec
//        - pop_back - wyciągnięcie z konca kolejki

public class MyQueue <T> {

    private ArrayList<T> queue = new ArrayList<>();

    public void pushBack(T element){
        try {
        queue.add(queue.size()-1, element);
        } catch (IndexOutOfBoundsException iobe) {
            queue.add(0, element);
        }
    }
    public T popFront(){
        T temp=queue.get(0);
        queue.remove(0);
        return (T) temp;

    }
    public T peek(int i){
        return queue.get(i);
    }
    public int size(){
        return queue.size();
    }
    public void pushFront(T element){
        queue.add(0, element);
    }
    public T popBack(){

        T temp=queue.get(queue.size()-1);
        queue.remove(queue.size()-1);
        return temp;
    }

}
