//Posługując się ArrayList'ą (jako polem klasy) zaimplementuj klasę MyPriorityQueue (bedzie Ci potrzebna klasa
// QueueElement), ktora posiada metody:
//        - push - dodanie do kolejki
//        - pop - wyciagniecie z kolejki
//        - size - zwraca wielkosc kolejki
//        - print - wypisuje elementy kolejki
//        *Kolejka powinna sortować elementy malejąco lub rosnąco w zależności od parametry konstruktora.
//

public class QueueElement<T> {
    private int priority;
    private T object;

    public QueueElement(int priority, T object) {
        this.priority = priority;
        this.object = object;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public T getObject() {
        return object;
    }

    public void setObject(T object) {
        this.object = object;
    }
}
