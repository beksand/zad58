import java.util.ArrayList;
import java.util.List;

public class MyPriorityQueue<T> {

    private List<QueueElement<T>> prQueue = new ArrayList<>();

    public void push(int pr, T element){
        QueueElement<T> nowy = new QueueElement<>(pr, element);
        for (int i = 0; i <prQueue.size() ; i++) {
            if (pr<prQueue.get(i).getPriority()){
                prQueue.add(i, (QueueElement<T>) element);
                break;
            }
        }
    }
    public T pop(int i){
        return (T) prQueue.get(i);
    }
}
